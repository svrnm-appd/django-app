from appdynamics.agent import api as appd
from django.db import connection

def appd_wrapper(request):
    bt_handle = appd.get_active_bt_handle(request)
    def real_wrapper(execute, sql, params, many, context):

        settings = context['connection'].settings_dict

        print(settings)

        name = "sql://{}:{}/{}".format(settings['HOST'], settings['PORT'], settings['NAME'])
        with appd.exit_call(bt_handle, appd.EXIT_DB, name, {
                'ENGINE': settings['ENGINE'],
                'NAME': settings['NAME'],
                'HOST': settings['HOST'],
                'PORT': settings['PORT']
            }, operation=sql):
            return execute(sql, params, many, context)

    return real_wrapper

class AppDynamicsMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        with connection.execute_wrapper(appd_wrapper(request)):
            return self.get_response(request)

CREATE DATABASE DemoData;
GO
USE DemoData;
GO
CREATE TABLE items (ID int, name nvarchar(max), description nvarchar(max));
GO
